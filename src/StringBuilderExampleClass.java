import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class StringBuilderExampleClass {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter line: ");
        String originalLine = scanner.nextLine();

        String[] words = originalLine.split(" ");
        Collections.addAll(list, words);
        System.out.println(getLine(list));
    }

    private static String getLine(List<String> words){
        StringBuilder stringBuilder;
        if(words.size() == 0){
            stringBuilder = new StringBuilder();
        }else {
            stringBuilder = new StringBuilder(words.get(0));
            words.remove(0);
        }

        while (words.size() > 0){
            for (int i = 0; i < words.size(); i++){
                String a = stringBuilder.toString().toLowerCase();
                String b = words.get(i).toLowerCase();

                if(a.charAt(stringBuilder.length() - 1) == b.charAt(0)){
                    stringBuilder.append(" ").append(words.get(i));
                    words.remove(i);
                    continue;
                }
            }
        }

        return stringBuilder.toString();
    }
}
