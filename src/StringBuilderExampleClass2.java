import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class StringBuilderExampleClass2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String fileName = sc.nextLine();

        List<String> list = new ArrayList<>();
        String[] s = fileName.split(" ");
        Collections.addAll(list, s);
        System.out.println(getLine(list));

    }

    private static String getLine(List<String> words){
        StringBuilder sb;

        if(words.size() == 0){
            sb = new StringBuilder();
        }else {
            sb = new StringBuilder(words.get(0));
            words.remove(0);
        }

        while (words.size() > 0){
            for(int i = 0; i < words.size(); i++){
                String a = words.get(i).toLowerCase();
                String b = sb.toString().toLowerCase();

                if(a.charAt(0) == b.charAt(sb.length() - 1)){
                    sb.append(" ").append(words.get(i));
                    words.remove(i);
                    continue;
                }

                if(b.charAt(0) == a.charAt(a.length() - 1)){
                    sb.insert(0, " ");
                    sb.insert(0, words.get(i));
                    words.remove(i);
                }
            }
        }

        return sb.toString();
    }
}
