import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class StringBuilderExampleClass3 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter line: ");

        String originalLine = scanner.nextLine();
        String[] words = originalLine.split(" ");
        Collections.addAll(list, words);

        System.out.println(getLine(list));
    }

    private static String getLine(List<String> words){
        String initialWord = words.get(0);

        for(int i = 0; i < words.size(); i++){
            if(initialWord.charAt(0) > words.get(i).charAt(0)){
                initialWord = words.get(i);
            }
        }

        StringBuilder sb = new StringBuilder(initialWord);

        for(int i = 0; i < words.size(); i++){
            if(initialWord.equals(words.get(i))){
                words.remove(i);
            }
        }

        while (words.size() > 0){
            for (int i = 0; i < words.size(); i++){
                String a = sb.toString().toLowerCase();
                String b = words.get(i).toLowerCase();

                if(a.charAt(sb.length() - 1) == b.charAt(0)){
                    sb.append(" ").append(words.get(i));
                    words.remove(i);
                    continue;
                }
            }
        }

        return sb.toString();
    }
}
